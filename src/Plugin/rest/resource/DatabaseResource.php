<?php

/**
 * @file
 * Contains \Drupal\trinoco_services\Plugin\rest\resource\DatabaseResource.
 */

namespace Drupal\trinoco_services\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Database;

/**
 * Provides a resource for database watchdog log entries.
 *
 * @RestResource(
 *   id = "database",
 *   label = @Translation("Get database"),
 *   uri_paths = {
 *     "canonical" = "/trinoco/database"
 *   }
 * )
 */
class DatabaseResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * Returns a watchdog log entry for the specified ID.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the log entry.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get() {
    $settings = \Drupal::configFactory()->getEditable('trinoco_services.settings')->get('eb_export');
    $databases = Database::getConnectionInfo();

    if (isset($settings['export']) && $settings['export'] == 'false') {
      $results = array(array('database' => 'Not enabled for exporting.'));
    }
    else if (!file_exists('public://trinoco-services-' . $databases['default']['database'] . '.sql.gz')) {
      $results = array(array('database' => 'Not exported yet.'));
    }
    else {
      $files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(array(
        'uri' => 'public://trinoco-services-' . $databases['default']['database'] . '.sql.gz'
      ));
      $file = reset($files);

      $results  = array(array('database' => array('timestamp' => $file->getCreatedTime(), 'uri' => file_create_url($file->getFileUri()))));
    }

    $response = new ResourceResponse($results);
    $response->addCacheableDependency(array(
      '#cache' => array(
        'max-age' => 0,
      ),
    ));

    return $response;
  }
}
