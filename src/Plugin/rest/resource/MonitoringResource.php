<?php

/**
 * @file
 * Contains \Drupal\trinoco_services\Plugin\rest\resource\MonitoringResource.
 */

namespace Drupal\trinoco_services\Plugin\rest\resource;

use Drupal\Core\Database\Database;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a resource for database watchdog log entries.
 *
 * @RestResource(
 *   id = "monitoring",
 *   label = @Translation("Get updates"),
 *   uri_paths = {
 *     "canonical" = "/trinoco/monitoring"
 *   }
 * )
 */
class MonitoringResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * Returns a watchdog log entry for the specified ID.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the log entry.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get() {
    $databases = Database::getConnectionInfo();
    $database_info = $this->getDatabaseInfo($databases);

    $mysqli = new \mysqli($databases['default']['host'], $databases['default']['username'], $databases['default']['password']);

    $results = array(
      'system' => array(
        'memory_limit' => ini_get('memory_limit'),
        'memory_usage' => $this->getMemoryUsage(),
        'processor_usage' => $this->get_server_cpu_usage(),
        'server_software' => $_SERVER['SERVER_SOFTWARE'],
        'free_disk_space' => $this->calculate_size(disk_free_space('/')),
        'total_disk_space' => $this->calculate_size(disk_total_space('/')),
        'db_usage' => $database_info['total_usage'] . "m",
        'php_version' => phpversion(),
        'mysql_version' => mysqli_get_server_info($mysqli),
        'os' => PHP_OS
      ),
      'database' =>
        array(
          'tables' => $database_info
        )
    );

    $response = new ResourceResponse($results);
    $response->addCacheableDependency(array(
      '#cache' => array(
        'max-age' => 0,
      ),
    ));

    return $response;
  }

  private function getDatabaseInfo($databases) {
    $result = db_query("SELECT table_name, SUM(TABLE_ROWS) as row_count
    FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_SCHEMA = '" . $databases['default']['database'] . "' 
    GROUP BY TABLE_NAME
    ORDER BY table_name ASC")->fetchAll();

    $db = array();
    foreach ($result as $key => $row) {
      $db[$row->table_name] = array(
        'rows' => $row->row_count
      );
    }

    $result2 = db_query("SELECT table_name, round(((data_length + index_length) / 1024 / 1024), 2) as data_used 
    FROM information_schema.TABLES
    where table_schema = '" . $databases['default']['database'] . "'")->fetchAll();

    $db_usage = 0;
    foreach ($result2 as $key => $row) {
      $db_usage += $row->data_used;
      $db[$row->table_name]['Used data'] = $row->data_used;
    }

    $db['total_usage'] = $db_usage;

    return $db;
  }

  private function calculate_size($Bytes)
  {
    $Type=array("", "k", "m", "g", "t", "p", "e", "z", "y");
    $Index=0;
    while($Bytes>=1024)
    {
      $Bytes/=1024;
      $Index++;
    }
    $Bytes = round($Bytes, 2);
    return("".$Bytes.$Type[$Index]."b");
  }

  private function getMemoryUsage()
  {
    $size = memory_get_usage(TRUE);
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
  }

  private function get_server_cpu_usage(){

    $load = sys_getloadavg();
    return $load[0];
  }
}
