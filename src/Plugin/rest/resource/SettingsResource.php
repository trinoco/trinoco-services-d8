<?php

/**
 * @file
 * Contains \Drupal\trinoco_services\Plugin\rest\resource\SettingsResource.
 */

namespace Drupal\trinoco_services\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a resource for database watchdog log entries.
 *
 * @RestResource(
 *   id = "settings",
 *   label = @Translation("Get settings"),
 *   uri_paths = {
 *     "canonical" = "/trinoco/settings"
 *   }
 * )
 */
class SettingsResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * Returns a watchdog log entry for the specified ID.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the log entry.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get() {
    $parameters = \Drupal::request()->query->get('parameters');
    $config = \Drupal::configFactory()->getEditable('trinoco_services.settings');
    $results = array(
      'settings' => $config->get('settings')
    );

    foreach ($parameters as $setting => $value) {
      $results['settings'][$setting] = $value;
    }

    $config->set('settings', $results['settings']);
    $config->save();

    $response = new ResourceResponse($results);
    $response->addCacheableDependency(array(
      '#cache' => array(
        'max-age' => 0,
      ),
    ));

    return $response;
  }
}
