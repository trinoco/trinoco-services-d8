<?php

/**
 * @file
 * Contains \Drupal\trinoco_services\Plugin\rest\resource\UpdateResource.
 */

namespace Drupal\trinoco_services\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a resource for database watchdog log entries.
 *
 * @RestResource(
 *   id = "update",
 *   label = @Translation("Get updates"),
 *   uri_paths = {
 *     "canonical" = "/trinoco/updates"
 *   }
 * )
 */
class UpdateResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * Returns a watchdog log entry for the specified ID.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the log entry.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get() {
    $available = update_get_available(TRUE);
    $data = update_calculate_project_data($available);

    $results = array();
    foreach ($data as $project => $info) {
      if ($project == 'drupal') {
        $info['info']['name'] = 'drupal';
      }
      $results[$project] = array(
        'name' => $info['info']['name'],
        'current_version' => $info['info']['version'],
      );

      $url = 'https://updates.drupal.org/release-history/' . $project . '/8.x';
      $xml = simplexml_load_file($url);

      $array = xml2array($xml);

      $releases = array();

      foreach ($array['releases']['release'] as $release) {
        $release = xml2array($release);

        if (strrpos($release['version'], 'dev') === FALSE) {
          $releases[$release['date']]['version'] = $release['version'];

          if (!isset($release['terms']['term'][0])) {
            $releases[$release['date']]['terms'][] = $release['terms']['term']['value'];
          }
          else {
            foreach ($release['terms']['term'] as $term) {
              $term = xml2array($term);
              $releases[$release['date']]['terms'][] = $term['value'];
            }
          }
        }
      }
      ksort($releases);

      foreach ($releases as $key => $release) {
        unset($releases[$key]);
        if ($release['version'] == $results[$project]['current_version']) {
          break;
        }
      }

      foreach ($releases as $release) {
        $results[$project]['latest_version'] = $release['version'];

        if (in_array('Security update', $release['terms'])) {
          $results[$project]['type'] = 'Security update';
        }

        if (in_array('New features', $release['terms'])
          && !isset($results[$project]['type'])
        || in_array('Bug fixes', $release['terms'])
        && !isset($results[$project]['type'])) {
          $results[$project]['type'] = $release['terms'][0];
        }
      }
    }

    $response = new ResourceResponse($results);
    $response->addCacheableDependency(array(
      '#cache' => array(
        'max-age' => 0,
      ),
    ));

    return $response;
  }
}

function xml2array ( $xmlObject)
{
  $out = array();
  foreach ( (array) $xmlObject as $index => $node )
    $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

  return $out;
}
